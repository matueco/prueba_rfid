
/*==================[inclusions]=============================================*/

#include "rfid.h"
#include "board.h"
#include "gpio_18xx_43xx.h"
#include "ssp_18xx_43xx.h"


/*==================[macros and definitions]=================================*/



/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

static void initHardware(void);

/*==================[internal functions definition]==========================*/


//uint8_t SPITransfer(uint8_t val)
//{
//    Chip_SSP_DATA_SETUP_T xferConfig;
//
//    uint8_t txbuf[1]={val};
//    uint8_t rxbuf[1];
//
//	xferConfig.tx_data = val;
//	xferConfig.tx_cnt  = 0;
//	xferConfig.rx_data = rxbuf;
//	xferConfig.rx_cnt  = 0;
//	xferConfig.length  = sizeof(val);
//
//	Chip_SSP_RWFrames_Blocking(LPC_SSP1, &xferConfig);
//
//	return rxbuf;
//}


uint8_t SPITransfer(uint8_t *bufferTx) //, uint8_t *bufferRx, uint32_t bufferLen)
{
	Chip_SSP_DATA_SETUP_T spiSetup;

	uint8_t bufferRx[2];

	spiSetup.tx_data = bufferTx;
	spiSetup.rx_data = bufferRx;
	spiSetup.tx_cnt=0;
	spiSetup.rx_cnt=0;
	spiSetup.length=1;

	Chip_SSP_RWFrames_Blocking(LPC_SSP1, &spiSetup);

	return bufferRx[0];

}


/*
 * Function Name: Write_AddicoreRFID
 * Function Description: To a certain AddicoreRFID register to write a byte of data
 * Input Parameters: addr - register address; val - the value to be written
 * Return value: None
 */
static void MFRC522_Wr( char addr, char value )
{
	    uint8_t buf;

		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 3, 0) ; //Selects the SPI coneected RFID modulo a low
		//Address Format: 0XXXXXX0, the left most "0" indicates a write
		buf= ( addr << 1 ) & 0x7E;
		SPITransfer( &buf);
		buf = value;
        SPITransfer( &buf );

        Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 3, 0); //Selects the SPI coneected RFID modulo a high
}



//Lee y retorna un byte del registro del RFID
static char MFRC522_Rd( char addr )
{
		char value;
		uint8_t buf;

		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 3, 0) ; //LOW

		//Address Format: 1XXXXXX0, the first "1" indicates a read
		//SPITransfer( (( addr << 1 ) & 0x7E) | 0x80 );
		buf= ( addr << 1 ) & 0x7E;
		SPITransfer( &buf);
        value = SPITransfer( 0x00 ); //Leer lo que recibo

        Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 3, 0); //HIGH

        return value;
}


//VER
static void MFRC522_Clear_Bit( char addr, char mask )
{
//		char tmp;
//	    tmp = Read_AddicoreRFID(reg);
//	    Write_AddicoreRFID(reg, tmp | mask);
     MFRC522_Wr( addr, MFRC522_Rd( addr ) & (~mask) ); //Set bit mask
}


//VER
static void MFRC522_Set_Bit( char addr, char mask )
{
//		char tmp;
//	    tmp = Read_AddicoreRFID(reg);
//	    Write_AddicoreRFID(reg, tmp | mask);
     MFRC522_Wr( addr, MFRC522_Rd( addr ) | mask );
}



void MFRC522_Reset()
{
//		char temp;
//
//		temp = Read_AddicoreRFID(TxControlReg);
//		if (!(temp & 0x03))
//		{
//			SetBitMask(TxControlReg, 0x03);
//		}
        MFRC522_Wr( COMMANDREG, PCD_RESETPHASE );
}


/*
 * Function Name: AntennaOn
 * Description: Open antennas, each time you start or shut down the natural barrier between the transmitter should be at least 1ms interval
 * Input: None
 * Return value: None
 */

void MFRC522_AntennaOn()
{
 	MFRC522_Set_Bit( TXCONTROLREG, 0x03 );
}


/*
  * Function Name: AntennaOff
  * Description: Close antennas, each time you start or shut down the natural barrier between the transmitter should be at least 1ms interval
  * Input: None
  * Return value: None
 */
void MFRC522_AntennaOff()
{
 MFRC522_Clear_Bit( TXCONTROLREG, 0x03 );
}


/*
 * Function Name: AddicoreRFID_Init
 * Description: Initialize the AddicoreRFID module
 * Input: None
 * Return value: None
*/
void MFRC522_Init()
{

   	 Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 3, 0); // Selects the SPI connected AddicoreRFID module

//   	 if (digitalRead(_resetPin) == LOW) { //The AddicoreRFID is in power down mode
//   			digitalWrite(_resetPin, HIGH);	// Hard reset AddicoreRFID
//   			// Allows for 37.74us oscillator start-up delay
//   		}
//   		else {
//   			AddicoreRFID_Reset();           // Soft reset the AddicoreRFID
//   		}
//

	 MFRC522_Reset(); //Soft reset


     MFRC522_Wr( TMODEREG, 0x8D );      //Tauto=1; f(Timer) = 6.78MHz/TPreScaler
     MFRC522_Wr( TPRESCALERREG, 0x3E ); //TModeReg[3..0] + TPrescalerReg
     MFRC522_Wr( TRELOADREGL, 30 );
     MFRC522_Wr( TRELOADREGH, 0 );

     MFRC522_Wr( TXAUTOREG, 0x40 );    //100%ASK
     MFRC522_Wr( MODEREG, 0x3D );      // CRC valor inicial de 0x6363

     //MFRC522_Clear_Bit( STATUS2REG, 0x08 );//MFCrypto1On=0
     //MFRC522_Wr( RXSELREG, 0x86 );      //RxWait = RxSelReg[5..0]
     //MFRC522_Wr( RFCFGREG, 0x7F );     //RxGain = 48dB

     MFRC522_AntennaOn(); //Open antena

}



/*
 * Function Name: AddicoreRFID_ToCard
 * Description: RC522 and ISO14443 card communication
 * Input Parameters: command - MF522 command word,
 *			 sendData--RC522 sent to the card by the data
 *			 sendLen--Length of data sent
 *			 backData--Data returned from the card
 *			 backLen--Returned data bit length
 * Return value: the successful return MI_OK
 */
char MFRC522_ToCard( char command, char *sendData, char sendLen, char *backData, int *backLen )
{
  char _status = MI_ERR;
  char irqEn = 0x00;
  char waitIRq = 0x00;
  char lastBits;
  char n;
  int i;

  switch (command)
  {
    case PCD_AUTHENT:       //Certification cards close
    {
      irqEn = 0x12;
      waitIRq = 0x10;
      break;
    }
    case PCD_TRANSCEIVE:    //Transmit FIFO data
    {
      irqEn = 0x77;
      waitIRq = 0x30;
      break;
    }
    default:
      break;
  }

  //ComIrqReg ver que va en lugar de commienreg
  MFRC522_Wr( COMMIENREG, irqEn | 0x80 );  //Interrupt request
  MFRC522_Clear_Bit( COMMIRQREG, 0x80 );   //Clear all interrupt request bit
  MFRC522_Set_Bit( FIFOLEVELREG, 0x80 );   //FlushBuffer=1, FIFO Initialization
  MFRC522_Wr( COMMANDREG, PCD_IDLE );      //NO action; Cancel the current command???


  //Writing data to the FIFO
  for ( i=0; i < sendLen; i++ )
  {
    MFRC522_Wr( FIFODATAREG, sendData[i] );
  }
  //Execute the command
  MFRC522_Wr( COMMANDREG, command );
  if (command == PCD_TRANSCEIVE )
  {
    MFRC522_Set_Bit( BITFRAMINGREG, 0x80 ); //StartSend=1,transmission of data starts
  }
  //Waiting to receive data to complete
  //i according to the clock frequency adjustment, the operator M1 card maximum waiting time 25ms???
  i = 0xFFFF; //delay
  do
  {
    //CommIrqReg[7..0]
    //Set1 TxIRq RxIRq IdleIRq HiAlerIRq LoAlertIRq ErrIRq TimerIRq
    n = MFRC522_Rd( COMMIRQREG );
    i--;
  }
  while ( i && !(n & 0x01) && !( n & waitIRq ) );
  MFRC522_Clear_Bit( BITFRAMINGREG, 0x80 );    //StartSend=0
  if (i != 0)
  {
    if( !( MFRC522_Rd( ERRORREG ) & 0x1B ) ) //BufferOvfl Collerr CRCErr ProtecolErr
    {
      _status = MI_OK;
      if ( n & irqEn & 0x01 )
      {
        _status = MI_NOTAGERR;       //??
      }
      if ( command == PCD_TRANSCEIVE )
      {
        n = MFRC522_Rd( FIFOLEVELREG );
        lastBits = MFRC522_Rd( CONTROLREG ) & 0x07;
        if (lastBits)
        {
          *backLen = (n-1) * 8 + lastBits;
        }
        else
        {
          *backLen = n * 8;
        }
        if (n == 0)
        {
          n = 1;
        }
        if (n > 16)
        {
          n = 16;
        }
        //Reading the received data in FIFO
        for (i=0; i < n; i++)
        {
          backData[i] = MFRC522_Rd( FIFODATAREG );
        }

      backData[i] = 0; //Ver si va
      }
    }
    else
    {
      _status = MI_ERR;
    }
  }
  //MFRC522_Set_Bit( CONTROLREG, 0x80 );
  //MFRC522_Wr( COMMANDREG, PCD_IDLE );
  return _status;
}



/*
 * Function Name: AddicoreRFID_Request
 * Description: Find cards, read the card type number
 * Input parameters: reqMode - find cards way
 *			 TagType - Return Card Type
 *			 	0x4400 = Mifare_UltraLight
 *				0x0400 = Mifare_One(S50)
 *				0x0200 = Mifare_One(S70)
 *				0x0800 = Mifare_Pro(X)
 *				0x4403 = Mifare_DESFire
 * Return value: the successful return MI_OK
 */
char MFRC522_Request( char reqMode, char *TagType )
{
  char _status;
  int backBits;            //The received data bits
  MFRC522_Wr( BITFRAMINGREG, 0x07 ); //TxLastBists = BitFramingReg[2..0]   ???
  TagType[0] = reqMode;
  _status = MFRC522_ToCard( PCD_TRANSCEIVE, TagType, 1, TagType, &backBits );
  if ( (_status != MI_OK) || (backBits != 0x10) )
  {
    _status = MI_ERR;
  }
  return _status;
}

/*
 * Function Name: CalulateCRC
 * Description: CRC calculation with MF522
 * Input parameters: pIndata - To read the CRC data, len - the data length, pOutData - CRC calculation results
 * Return value: None
 */
void MFRC522_CRC( char *dataIn, char length, char *dataOut )
{
	char i, n;
    MFRC522_Clear_Bit( DIVIRQREG, 0x04 );
    MFRC522_Set_Bit( FIFOLEVELREG, 0x80 );

 //Escreve dados no FIFO
    for ( i = 0; i < length; i++ )
    {
        MFRC522_Wr( FIFODATAREG, *dataIn++ );
    }

    MFRC522_Wr( COMMANDREG, PCD_CALCCRC );

    i = 0xFF;
    //Espera a finalização do Calculo do CRC
    do
    {
        n = MFRC522_Rd( DIVIRQREG );
        i--;
    }
    while( i && !(n & 0x04) );        //CRCIrq = 1
        
    dataOut[0] = MFRC522_Rd( CRCRESULTREGL );
    dataOut[1] = MFRC522_Rd( CRCRESULTREGM );
}


/*
 * Function Name: AddicoreRFID_SelectTag
 * Description: Selection card, read the card memory capacity
 * Input parameters: serNum - Incoming card serial number
 * Return value: the successful return of card capacity
 */
char MFRC522_SelectTag( char *serNum )
{
  char i;
  char _status;
  char size;
  int recvBits;
  char buffer[9];

  //MFRC522_Clear_Bit( STATUS2REG, 0x08 );   //MFCrypto1On=0

  buffer[0] = PICC_SElECTTAG;
  buffer[1] = 0x70;

  for ( i=2; i < 7; i++ )
  {
    buffer[i] = *serNum++;
  }

  MFRC522_CRC( buffer, 7, &buffer[7] );

  _status = MFRC522_ToCard( PCD_TRANSCEIVE, buffer, 9, buffer, &recvBits );
  if ( (_status == MI_OK) && (recvBits == 0x18) )
  {
    size = buffer[0];
  }
  else
  {
    size = 0;
  }
  return size;
}

/*
 * Function Name: AddicoreRFID_Halt
 * Description: Command card into hibernation
 * Input: None
 * Return value: None
 */
void MFRC522_Halt()
{
  int unLen;
  char buff[4];

  buff[0] = PICC_HALT;
  buff[1] = 0;
  MFRC522_CRC( buff, 2, &buff[2] );
  MFRC522_Clear_Bit( STATUS2REG, 0x80 ); //
  MFRC522_ToCard( PCD_TRANSCEIVE, buff, 4, buff, &unLen );//
  MFRC522_Clear_Bit( STATUS2REG, 0x08 );// Puede que no vayan
}


/*
 * Function Name: AddicoreRFID_Auth
 * Description: Verify card password
 * Input parameters: authMode - Password Authentication Mode
                 0x60 = A key authentication
                 0x61 = Authentication Key B
             BlockAddr--Block address
             Sectorkey--Sector password
             serNum--Card serial number, 4-byte
 * Return value: the successful return MI_OK
 */
char MFRC522_Auth( char authMode, char BlockAddr, char *Sectorkey, char *serNum )
{
  char _status;
  int recvBits;
  char i;
  char buff[12];

  //Verify the command block address + sector + password + card serial number
  buff[0] = authMode;
  buff[1] = BlockAddr;

  for ( i = 2; i < 8; i++ )
  {
    buff[i] = Sectorkey[i-2];
  }

  for ( i = 8; i < 12; i++ )
  {
    buff[i] = serNum[i-8];
  }

  _status = MFRC522_ToCard( PCD_AUTHENT, buff, 12, buff, &recvBits );

  if ( ( _status != MI_OK ) || !( MFRC522_Rd( STATUS2REG ) & 0x08 ) )
  {
    _status = MI_ERR;
  }

  return _status;
}


/*
 * Function Name: AddicoreRFID_Write
 * Description: Write block data
 * Input parameters: blockAddr - block address; writeData - to 16-byte data block write
 * Return value: the successful return MI_OK
 */
char MFRC522_Write( char blockAddr, char *writeData )
{
  char _status;
  int recvBits;
  char i;
  char buff[18];
  buff[0] = PICC_WRITE;
  buff[1] = blockAddr;

  MFRC522_CRC( buff, 2, &buff[2] );
  _status = MFRC522_ToCard( PCD_TRANSCEIVE, buff, 4, buff, &recvBits );
  if ( (_status != MI_OK) || (recvBits != 4) || ( (buff[0] & 0x0F) != 0x0A) )
  {
    _status = MI_ERR;
  }
  if (_status == MI_OK)
  {
    for ( i = 0; i < 16; i++ )                //Data to the FIFO write 16Byte
    {
      buff[i] = writeData[i];
    }

    MFRC522_CRC( buff, 16, &buff[16] );
    _status = MFRC522_ToCard( PCD_TRANSCEIVE, buff, 18, buff, &recvBits );
    if ( (_status != MI_OK) || (recvBits != 4) || ( (buff[0] & 0x0F) != 0x0A ) )
    {
      _status = MI_ERR;
    }
  }
  return _status;
}


/*
 * Function Name: AddicoreRFID_Read
 * Description: Read block data
 * Input parameters: blockAddr - block address; recvData - read block data
 * Return value: the successful return MI_OK
 */
char MFRC522_Read( char blockAddr, char *recvData )
{
  char _status;
  int unLen;
  recvData[0] = PICC_READ;
  recvData[1] = blockAddr;

  MFRC522_CRC( recvData, 2, &recvData[2] );

  _status = MFRC522_ToCard( PCD_TRANSCEIVE, recvData, 4, recvData, &unLen );
  if ( (_status != MI_OK) || (unLen != 0x90) )
  {
    _status = MI_ERR;
  }
  return _status;
}


/*
 * Function Name: AddicoreRFID_Anticoll
 * Description: Anti-collision detection, reading selected card serial number card
 * Input parameters: serNum - returns 4 bytes card serial number, the first 5 bytes for the checksum byte
 * Return value: the successful return MI_OK
 */
char MFRC522_AntiColl( char *serNum )
{
  char _status;
  char serNumCheck = 0;
  int unLen, i;

  MFRC522_Wr( BITFRAMINGREG, 0x00 ); //TxLastBists = BitFramingReg[2..0]
  serNum[0] = PICC_ANTICOLL;
  serNum[1] = 0x20;
  MFRC522_Clear_Bit( STATUS2REG, 0x08 );
  _status = MFRC522_ToCard( PCD_TRANSCEIVE, serNum, 2, serNum, &unLen );
  if (_status == MI_OK)
  {
    for ( i=0; i < 4; i++ )
    {
      serNumCheck ^= serNum[i];
    }

    if ( serNumCheck != serNum[4] )
    {
      _status = MI_ERR;
    }
  }
  return _status;
}

//0x0044 = Mifare_UltraLight
//0x0004 = Mifare_One (S50)
//0x0002 = Mifare_One (S70)
//0x0008 = Mifare_Pro (X)
//0x0344 = Mifare_DESFire
char MFRC522_isCard( char *TagType )
{
    if (MFRC522_Request( PICC_REQIDL, TagType ) == MI_OK)
        return 1;
    else
        return 0;
}

char MFRC522_ReadCardSerial( char *str )
{
char _status;
 _status = MFRC522_AntiColl( str );
 str[5] = 0;
 if (_status == MI_OK)
  return 1;
 else
  return 0;
}


static void initHardware(void)
{
    Board_Init();
//    SystemCoreClockUpdate();
//    SysTick_Config(SystemCoreClock / 1000);

    /* SPI configuration */
    Board_SSP_Init(LPC_SSP1);
    Chip_SSP_Init(LPC_SSP1);
    Chip_SSP_Enable(LPC_SSP1);

    Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 3, 0); //GPIO0 --> SDA Puerto como salida
    Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 3, 0);
//    Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 3, 3); //GPIO3	--> RST
//    Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 3, 3);


    MFRC522_Init();
}


/*==================[external functions definition]==========================*/

int main ()
{

	//char UID[6], TagType;
	char status, str[16];
	//unsigned char checksum1;

	initHardware();
	uint8_t a;

	     while(1)
	     {
	    	 a=150;
	    	 SPITransfer(&a);
	    	 a=23;
	    	 SPITransfer(&a);

	      status = MFRC522_Request(PICC_REQIDL, str);

	      if (status == MI_OK)
    	        Board_LED_Toggle(0); //Prendo led si detecto tag

	      	//Anti-collision, return tag serial number 4 bytes
//	      	status = MFRC522_AntiColl(str);
//	      	if (status == MI_OK)
//	      	{
//	                checksum1 = str[0] ^ str[1] ^ str[2] ^ str[3];
//
//             }

	      	MFRC522_Halt();

	     //Verifico si hay alguna tarjeta
//	     if( MFRC522_isCard( &TagType ) )
//	     {
//	         //lectura del numero de serie
//	         if( MFRC522_ReadCardSerial( UID ) ) //SAque el &UID
//	         {
//	        	 Board_LED_Toggle(0);
//
//	         }
//
//
//	         MFRC522_Halt();
//	     }
	     }



return 0;
}


/*==================[end of file]============================================*/
